import React, {Component} from "react";
import Button from '@material-ui/core/Button';
import Traductor from './Traductor';
import Login from './Login';
import Register from './Register';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import logo from '../../ressources/LOGOBlanc.png'
import * as body from "@material-ui/system"
import { Typography } from "@material-ui/core";



    const home = {
        textAlign: "center",
        backgroundColor: "#2E353F",
        height : "100vh"

    }

    const button_guest = {
        color: "white",
       backgroundColor: "#0F7DFF",
       borderRadius: 30,
        right:50,



    }
    const button_login = {
        color: "white",
       backgroundColor: "#0F7DFF",
       borderRadius: 30,

    }
    const button_register = {
        color: "white",
       backgroundColor: "#0F7DFF",
       borderRadius: 30,
        left:50,

    }

    const navbar = {
        backgroundColor:  "#2E353F",
        height: "15vh"
    }

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guestMode:false,
            loginMode:false,
            registerMode:false
        }
      }

      openGuestMode(){
          this.setState({
            guestMode:true,
            loginMode:false,
            registerMode:false
          })
      } 

      openLoginMode(){
        this.setState({
          loginMode:true,
          guestMode:false,
          registerMode:false
        })
    } 

    openRegisterMode(){
       this.setState({
          loginMode:false,
          guestMode:false,
          registerMode:true
        })
    } 

    backToHome(){
        this.setState({
            loginMode:false,
            guestMode:false,
            registerMode:false
          })
    }

    render(){
        return (
            <div>
                {!this.state.guestMode && !this.state.loginMode && !this.state.registerMode &&
                <div style={home}>
                    <div><img src={logo} alt="logo" style={{width: "300px" ,paddingTop:50}}/></div>
                    <Button variant="contained" style={button_guest}   onClick={this.openGuestMode.bind(this)}>Guest</Button>
                    <Button variant="contained" style={button_login}   onClick={this.openLoginMode.bind(this)}>Login</Button>
                    <Button variant="contained" style={button_register}   onClick={this.openRegisterMode.bind(this)}>Register</Button>
                </div>}


                {(this.state.guestMode || this.state.loginMode || this.state.registerMode) &&
                <div>
                    <div class="container" style={navbar}>
                        <Button onClick={this.backToHome.bind(this)} style={{paddingTop: "5vh"}}>
                            <ArrowBackIosIcon style={{color: "white"}}/>
                        </Button>
                        <Typography style={{textAlign: "center", color: "white", marginTop:-25}}>GUEST</Typography>
                    </div>
                    <Traductor isOpen={this.state.guestMode}/>
                    <Login isOpen={this.state.loginMode}/>
                    <Register isOpen={this.state.registerMode}/>
                </div>}

            </div>
        )

    }
}

export default Home;
