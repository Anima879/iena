import React, {Component} from "react";
import Button from '@material-ui/core/Button';
import axiosInstance from '../axios';
import logo from "../../ressources/LOGOBlanc.png";

const traductor = {
    textAlign: "center",
    marginTop: 50
}
const upload = {
    color: "white",
    backgroundColor: "#0F7DFF",
    borderRadius: 30,
    top: 30
}

class Traductor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedImage: null,
            imageURL: null,
            value: null
        };

        this.fileSelectedHandler = this.fileSelectedHandler.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleSubmit() {
        // Perform image verification and upload
        if (this.checkImage()) {
            // payload
            let formData = new FormData();
            formData.append('image', this.state.selectedImage)
            axiosInstance.post(`upload/`, formData).then(
                (response) => {
                    this.setState({value: response['data']['value']})
                }
            );
        }
    }

    fileSelectedHandler = event => {
        this.setState({
            selectedImage: event.target.files[0],
            imageURL: URL.createObjectURL(event.target.files[0])
        });
    }

    checkImage() {
        const image = document.getElementById("image");
        if (image.clientHeight === 94 && image.clientWidth === 64) {
            return true
        } else {
            alert("Image size must be 64x64")
            return false
        }
    }


    render() {
        return (
            <div>
                {this.props.isOpen &&
                <div style={traductor}>
                    <form>
                        <input type="file" id="file" onChange={this.fileSelectedHandler} accept="image/png,image/jpeg"/>
                    </form>
                    {this.state.selectedImage != null &&
                    <div>
                        <div><img id="image" src={this.state.imageURL} style={{paddingTop: "30px"}} alt={'Uploaded image'}/></div>
                        <div><Button style={upload} variant="contained" onClick={this._handleSubmit}>Upload</Button>
                        </div>
                        {this.state.value != null &&
                        <div style={{marginTop: 60}}>Value: {this.state.value} </div>}
                    </div>
                    }


                </div>}
            </div>
        )
    }

}

export default Traductor;

