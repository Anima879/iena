Application web de reconnaissance de caractères chinois manuscrits.
Réalisée avec Django et React.

<h2>Tester l'application localement</h2>
<h3>Dépendances</h3>
Pour les dépendances Python 3.8 ou plus :

```
pip install -r requirements.txt
```

Pour les dépendances Node.js :
```
cd frontend
``` 
(il faut être dans le dossier frontend)
```
npm install
```

<h2>Lancer le serveur</h2>
A la racine du projet :

```
python manage.py runserver
```

Le serveur est lancé sur la machine local.
La base de données doit être configurer dans WebApp/settings.py pour que le serveur fonctionne pleinement.
