import os
import PIL
import matplotlib.pyplot as plt
import numpy as np
import pickle
from typing import Union
from django.conf import settings
import tensorflow as tf

from api.ChineseClassifier.dataset_loader import DatasetLoader

SAVED_MODELS_DIR = os.path.join(settings.BASE_DIR, "api/ChineseClassifier")

try:
    # Disable all GPUS
    tf.config.set_visible_devices([], 'GPU')
    visible_devices = tf.config.get_visible_devices()
    print(visible_devices)
    for device in visible_devices:
        assert device.device_type != 'GPU'
except Exception as e:
    # Invalid device or cannot modify virtual devices once initialized.
    print(e)


class AbstractClassifier:

    def __init__(self):
        pass

    def fit(self, epochs):
        raise NotImplementedError

    def plot_history(self):
        raise NotImplementedError

    def save_model(self, name):
        raise NotImplementedError

    def load_model(self, name):
        raise NotImplementedError

    def create_model(self):
        raise NotImplementedError


class ChineseNumberClassifier(AbstractClassifier):

    def __init__(self, dataset_loader: Union[DatasetLoader, None]):
        super().__init__()
        self.model = None
        self.history = None
        self.dataset_loader = dataset_loader
        self.classes_dict = self.dataset_loader.train_dataset.class_indices if dataset_loader is not None else None

    def fit(self, epochs):
        if self.model is None:
            raise ValueError("Model is not set")

        self.history = self.model.fit_generator(
            self.dataset_loader.train_dataset,
            epochs=epochs,
            validation_data=self.dataset_loader.validation_dataset)

    def predict(self, path_to_image):
        pic = PIL.Image.open(path_to_image).convert('L')

        pic_array = np.asarray(pic)
        pic_array = pic_array.reshape((64, 64, 1))
        feature = np.asarray([pic_array])

        classes = self.classes_dict

        predictions = self.model.predict(feature)

        val = np.argmax(predictions[0])

        for key, value in classes.items():
            if val == value:
                return key

        raise KeyError

    def save_model(self, name):
        self.model.save(f'models/{name}/', save_format='tf')
        with open(f'models/{name}/classes_dict', 'wb') as f:
            pickle.dump(self.classes_dict, f)

    def load_model(self, name):
        with tf.device("/cpu:0"):
            self.model = tf.keras.models.load_model(os.path.join(SAVED_MODELS_DIR, f"models/{name}"))
        with open(os.path.join(SAVED_MODELS_DIR, f"models/{name}/classes_dict"), 'rb') as f:
            self.classes_dict = pickle.load(f)

        self.model.summary()

    def plot_history(self):
        plt.plot(self.history.history['accuracy'], label="Accuracy (training data)")
        plt.plot(self.history.history['val_accuracy'], label="Accuracy (validation data)")

        plt.plot(self.history.history['loss'], label="Loss (training data)")
        plt.plot(self.history.history['val_loss'], label="Loss (validation data)")

        plt.xlabel("Number of epochs")
        plt.ylabel("Metrics value")
        plt.legend(loc="center right")

        plt.show()

    def create_model(self):
        self.model = tf.keras.models.Sequential([
            tf.keras.layers.Conv2D(16, 3, padding="same", activation="relu", input_shape=(64, 64, 1)),
            tf.keras.layers.MaxPool2D(),
            tf.keras.layers.Dropout(0.1),
            tf.keras.layers.Conv2D(32, 3, padding="same", activation="relu"),
            tf.keras.layers.MaxPool2D(),
            tf.keras.layers.Dropout(0.1),
            tf.keras.layers.Conv2D(64, 3, padding="same", activation="relu"),
            tf.keras.layers.MaxPool2D(),
            tf.keras.layers.Dropout(0.1),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(512, activation='relu'),
            tf.keras.layers.Dense(15, activation='softmax')
        ])

        with tf.device("/cpu:0"):
            self.model.compile(optimizer='adam',
                               loss='sparse_categorical_crossentropy',
                               metrics=['accuracy'])
        self.model.summary()


if __name__ == "__main__":
    classifier = ChineseNumberClassifier(None)
    classifier.load_model("2_no_aug_100e")

    pred = classifier.predict("input_1_1_10.jpg")
    print(pred)
