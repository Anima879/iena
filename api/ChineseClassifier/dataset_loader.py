import os
import shutil
from tqdm import tqdm
import tensorflow as tf


class DatasetLoader:
    """
    ABSTRACT
    """

    def __init__(self, folder_path, size=1000):
        self.folder_path = folder_path
        self.size = size

        self.train_dataset = None
        self.validation_dataset = None

    def load(self):
        raise NotImplementedError


class ChineseNumberDatasetLoader(DatasetLoader):
    IMAGE_SHAPE = (64, 64)
    TRAINING_DATA_DIR = "datasets/"

    def __init__(self, folder_path, size=1000):
        super().__init__(folder_path, size)

    def load(self, validation_split=0.2, horizontal_flip=False, vertical_flip=False, rotation_range=0):
        train_data_gen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255,
                                                                         validation_split=validation_split,
                                                                         horizontal_flip=horizontal_flip,
                                                                         vertical_flip=vertical_flip,
                                                                         rotation_range=rotation_range)

        self.train_dataset = train_data_gen.flow_from_directory(
            "datasets/",
            target_size=self.IMAGE_SHAPE,
            color_mode='grayscale',
            classes=None,
            class_mode='sparse',
            batch_size=32,
            subset='training',
            interpolation="nearest"
        )

        valid_data_gen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255,
                                                                         validation_split=validation_split,
                                                                         horizontal_flip=horizontal_flip,
                                                                         vertical_flip=vertical_flip,
                                                                         rotation_range=rotation_range)

        self.validation_dataset = valid_data_gen.flow_from_directory(
            "datasets/",
            target_size=self.IMAGE_SHAPE,
            color_mode='grayscale',
            classes=None,
            class_mode='sparse',
            batch_size=32,
            subset='validation',
            interpolation="nearest"
        )

    @staticmethod
    def generate_dataset_folder(origin_path):
        # create sub folder if needed
        for i in range(15):
            try:
                os.mkdir(f"datasets/{i}")
            except FileExistsError:
                raise FileExistsError("Clean the files before calling this method")

        for name in tqdm(os.listdir(origin_path)):
            category = int(name.split(".")[0].split("_")[-1]) - 1
            target_path = f"datasets/{category}/{name}"
            shutil.copyfile(origin_path + "/" + name, target_path)


if __name__ == '__main__':
    loader = ChineseNumberDatasetLoader('../Ressources/data')
    loader.load()
    print(loader.train_dataset.class_indices)
