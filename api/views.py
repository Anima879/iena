from rest_framework.decorators import api_view
from .ChineseNumberReco.classifier import ChineseNumberReco
from .serializers import ImageSerializer, GuestImagesSerializer
from .models import GuestImages
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from django.http import JsonResponse
import os

BASE_DIR = settings.BASE_DIR


@api_view(['POST'])
def upload_image(request):
    if not request.session.exists(request.session.session_key):
        request.session.create()

    print(request.session.session_key)
    if request.method == 'POST':
        print(request.data)

        if request.data['isGuest'] == 'true':
            data = request.data
            data['session_id'] = request.session.session_key
            serializer = GuestImagesSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response({"message": "Upload successfully for guest"}, status=status.HTTP_201_CREATED)
        else:
            serializer = ImageSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"message": "Upload successfully"}, status=status.HTTP_201_CREATED)
        return Response({"message": "Upload failed"}, status=status.HTTP_400_BAD_REQUEST)
    return Response({"message": "Method must be POST"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def recognition_guest(request):
    if not request.session.exists(request.session.session_key):
        request.session.create()

    chinese_number_reco = ChineseNumberReco()

    data = request.data
    data['session_id'] = request.session.session_key
    serializer = GuestImagesSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        image = serializer.data.get('image')[1:]
        value = chinese_number_reco.classifier.predict(os.path.join(BASE_DIR, image))

        data = {
            'value': value
        }

        return JsonResponse(data, status=status.HTTP_200_OK)
    return Response({"message": "Upload failed"}, status=status.HTTP_400_BAD_REQUEST)
