
from django.urls import path
from .views import upload_image, recognition_guest
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('upload/', recognition_guest)
]
