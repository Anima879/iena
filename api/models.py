from django.db import models

from django.db import models


class Image(models.Model):
    image = models.ImageField(upload_to='images')


class GuestImages(models.Model):
    image = models.ImageField(upload_to='guest', null=False)
    session_id = models.CharField(max_length=100)
