from rest_framework import serializers
from .models import Image, GuestImages


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'


class GuestImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuestImages
        fields = '__all__'
