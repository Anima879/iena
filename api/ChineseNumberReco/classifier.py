from ..ChineseClassifier.classifier import ChineseNumberClassifier


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ChineseNumberReco(metaclass=Singleton):
    def __init__(self):
        self.classifier = ChineseNumberClassifier(None)
        self.classifier.load_model("2_no_aug_100e")
